#!/usr/bin/env python
# -*- coding: utf-8 -*- #
# Copyright (C) 2020 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

DEVICES_TO_GET = {
    # Device : usage
    "Asus D16 Mainboard" : {
        "Usage": "Will be used to build the fastest RYF x86 computer possible for compiling Replicant faster",
    },
    "Asrock P55 Extreme" : {
        "Usage" : "To test the removal of the Management Engine firmware",
    },
    "Beagleboard" : {
        "Usage" : "Convert the beagleboard to the devicetree in Barebox (will not be done soon though)",
    },
    "A Pine64 or any other 64bit ARM SBC supported by upstream u-boot and Linux and that can run boot with only software" : {
        "Usage" : "Work on making Parabola armv7h usable on 64bit ARM computers with 32bit userspace",
    },
}

DEVICES_TO_GIVE = {
    # Device : infos
    "dib9000 compatible Digital TV card" : {
        "State" : "nonfree firmware required, 1 coil to repair?",
    },
    "Many Intel WiFi cards" : {
        "State" : "nonfree firmware required",
    },
}
