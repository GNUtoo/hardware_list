#!/usr/bin/env python
# -*- coding: utf-8 -*- #
# Copyright (C) 2020 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import os
import sys

# FIXME: Here it works if we use the Makefile as '.' is at the project
# top directory. If not it will fail and needs to be fixed.
sys.path.append(os.path.realpath('utils'))
from lspci import *

GPU_DRIVERS = []

# 0300 is the class for VGA compatible controller
# lspci -d "::0300" -n | awk '{print $3}'

GPUS = [
    {
        'vid_pid': '1002:4e50',
        'driver' : 'radeon',
        'family' : 'R300',
        'location': 'Somewhere inside the Acer TravelMate 4000 mainboard',
    },
    {
        'vid_pid' : '1002:9710',
        'driver' : 'radeon',
        'family' : 'R600',
        'location': 'Inside the Asus M4A785T-M mainboard chipset',
        'drmdb link': 'https://drmdb.emersion.fr/devices/a0a68e28377e',
    },
    {
        'vid_pid' : '1002:9802',
        'driver' : 'radeon',
        'family' : 'Evergreen',
        'location': 'Inside the Asrock E350M1 mainboard chipset',
    },
    {
        'vid_pid': '1002:990b',
        'driver' : 'radeon',
        'family' : 'Northern Islands',
        'location': 'Somewhere inside the Lenovo G505s mainboard',
        'drmdb link': 'https://drmdb.emersion.fr/devices/aa6832fbd630',
    },
    {
        'vid_pid': '1002:002b',
        'product_str' : 'Cape Verde PRO [Radeon HD 7750/8740 / R7 250E]',
        'driver' : 'radeon',
        'family' : 'Southern Islands',
        'location': 'On a PCIe card',
        'drmdb link': 'https://drmdb.emersion.fr/devices/5e90a3d0d44b',
    },
]

for gpu in GPUS:
    # Make it faster to populate GPUS by hand as the correct format can be
    # automatized
    gpu['vendor_id'] = str(gpu['vid_pid'].split(":")[0])
    gpu['product_id'] = str(gpu['vid_pid'].split(":")[1])
    gpu.__delitem__('vid_pid')

    # TODO: implement class IDs and use defines or something like that
    # or find the class ID automatically in PciDevice
    class_id = "0300" # VGA class ID

    # TODO: use a function for that
    if 'driver' not in gpu.keys():
        if gpu['vendor_id'] == "1002":
            gpu['driver'] = "radeon?"
        else:
            gpu['driver'] = "Unknown"

    pci_device  = PciDevice(gpu['vendor_id'], gpu['product_id'], class_id)

    lspci_format = pci_device.lspci()
    if not gpu.get('lspci', None):
        gpu['lspci'] = lspci_format
    if not gpu.get('vendor_str', None):
        gpu['vendor_str'] = pci_device.vendor_str()
    if not gpu.get('product_str', None):
        gpu['product_str'] = pci_device.product_str()

    # TODO:
    # - make sure we don't need this workaround anymore
    # - Make sure that the PciDevice class output real matches real lspci output in corner cases
    #   - Handle sub product and vendor IDs, etc
    #   - Understand why lspci works with 1002:002b but not our parser
    if not gpu['lspci'] and gpu['vendor_str'] and gpu['product_str']:
        gpu['lspci'] = "VGA compatible controller" + ": " + gpu['vendor_str'] + " " + gpu['product_str']

for gpu in GPUS:
    gpu_driver = gpu.get("driver", "Unknown")

    if gpu_driver not in GPU_DRIVERS:
        GPU_DRIVERS.append(gpu_driver)
