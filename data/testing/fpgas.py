#!/usr/bin/env python
# -*- coding: utf-8 -*- #
# Copyright (C) 2021 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

FPGAS = [
    {
        "Name" : "FOMU (Hacker version)",
        "Usage": "Used to test the symbiflow toolchain Parabola packages",
        "Formfactor": "USB key",
        "FPGA" : "iCE40 UP5K",
    },
    {
        "Name" : "FOMU (PVT 1 version)",
        "Usage": "Used to test the symbiflow toolchain Parabola packages",
        "Formfactor": "USB key",
        "FPGA" : "iCE40 UP5K",
    },
    {
        "Name" : "XTRX CS",
        "Usage": "Building a test infrastructure with OsmoGSMTester (through cables) for Replicant",
        "Formfactor": "mPCIe card full height",
        "FPGA": "Xilinx Artix7 35T",
    },
]
