#!/usr/bin/env python
# -*- coding: utf-8 -*- #
# Copyright (C) 2020 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

COMPUTERS = [
    ########
    # SBCs #
    ########
    {
        "Name" : "LinkSprite pcDuino Lite",
        "Usage": "Used to do various tests",
        "Formfactor": "SBC",
        "SOC" : "Allwinner A10",
        "Bootloader": "Free u-boot from Parabola",
        "RAM" : "512M",
    },
    {
        "Name" : "Olimex Lime2 A20 eMMC",
        "Usage": "Used to do various tests",
        "Formfactor": "SBC",
        "SOC": "Allwinner A20",
        "Bootloader": "Free u-boot from Parabola",
        "RAM" : "1G",
    },
    {
        "Name" : "Hardkernel Odroid U3",
        "Usage": "For potential work on u-boot",
        "Formfactor": "SBC",
        "SOC": "Exynos 4412",
        "Bootloader": "U-Boot with several nonfree and unsigned components (BL1, TrustZone, etc)",
        "Debug tools": "Hacked serial cable, I need a less dangerous one.",
        "Accessories": "Case, PSU, eMMC, I've no micro-HDMI adapter",
    },
    {
        "Name" : "Texas Instruments Beaglebone Black",
        "Formfactor": "SBC",
        "SOC": "AM335x",
        "Bootloader": "Free u-boot from Parabola",
        "RAM" : "512M",
    },
    {
        "Name" : "Seed Studio Beaglebone Green",
        "Formfactor": "SBC",
        "SOC": "AM335x",
        "Bootloader": "Free u-boot from Parabola",
        "RAM" : "512M",
    },
    {
        "Name" : "BeagleBoard xM",
        "Formfactor": "SBC",
        "SOC": "OMAP3",
        "Bootloader": "Free u-boot from Parabola",
        "RAM" : "512M",
    },
    {
        "Name" : "Libre Computer AML-S805X-AC",
        "Formfactor": "SBC",
        "SOC": "Amlogic S805X",
        "Bootloader": "U-Boot with several nonfree and unsigned components (BL1, TrustZone, etc)",
        "RAM" : "1024M",
    },
    {
        "Name" : "PandaBoard",
        "Formfactor": "SBC",
        "SOC": "OMAP4",
        "Bootloader": "Free u-boot from Parabola",
    },
    {
        "Name" : "TBS TBS2910",
        "Usage": "Building Parabola packages, testing",
        "Formfactor": "SBC",
        "SOC" : "NXP I.MX6Q",
        "Bootloader": "Free u-boot from Parabola",
        "RAM" : "2G",
    },

    ###############
    # Smartphones #
    ###############
    # Ordered by vendor, product
    {
        "Name" : "Goldelico GTA04 A3",
        "Formfactor": "Smartphone",
        "Debug tools": "Serial cable",
        "Bootloader": "Old free u-boot and xloader forks",
    },
    {
        "Name" : "Goldelico GTA04 A4",
        "Formfactor": "Smartphone",
        "Debug tools": "Serial cable",
        "Bootloader": "Old free u-boot and xloader forks",
    },
    {
        "Name" : "LG Optimus Black (P970)",
        "Formfactor": "Smartphone",
        "Debug tools": "No UART, booting u-boot on microSD (boots first on microSD due to hardware modification)",
        "Bootloader": "Stock nonfree bootloader, upstream u-boot and various u-boot forks available",
    },
    {
        "Name" : "Nokia N900",
        "Formfactor": "Smartphone",
        "Debug tools": "UART, RX or TX, not both (non intrusive setup that is hard to use)",
        "Bootloader": "Stock nonfree and signed bootloader",
    },
    {
        "Name" : "Openmoko GTA01",
        "Formfactor": "Smartphone",
        "Debug tools": "UART, JTAG",
        "Bootloader": "Old free u-boot"
    },
    {
        "Name" : "Openmoko GTA02",
        "Formfactor": "Smartphone",
        "Debug tools": "UART, JTAG",
        "Bootloader": "Old free u-boot and/or free Qi bootloaders"
    },
    {
        "Name" : "Samsung Galaxy Nexus (GT-I9250)",
        "Formfactor": "Smartphone",
        "Debug tools": "UART",
        "SOC" : "OMAP4460",
        "Bootloader": "Stock nonfree and signed bootloader",
    },
    {
        "Name" : "Samsung Galaxy S (GT-I9000)",
        "Formfactor": "Smartphone",
        "TODO": "I need to reconstruct the EFS from scratch",
        "Debug tools": "UART",
        "Bootloader": "Stock nonfree and signed bootloader",
    },
    {
        "Name" : "Samsung Galaxy S II (GT-I9100)",
        "Formfactor": "Smartphone",
        "Debug tools": "UART",
        "SOC": "Exynos 4210",
        "Bootloader": "Stock nonfree and signed bootloader",
    },
    {
        "Name" : "Samsung Galaxy S II (GT-I9100G_CHN_CHN)",
        "Formfactor": "Smartphone",
        "Bootloader" : "Stock nonfree and signed Android 2.3.6 bootloader",
        "Debug tools": "UART, computer with USB controller compatible with that bootloader heimdall interface",
        "SOC" : "OMAP4430",
    },
    {
        "Name" : "Samsung Galaxy SIII (GT-I9300)",
        "Bootloader" : "Stock nonfree and signed bootloader (S-Boot 4.0)",
        "Formfactor": "Smartphone",
        "Debug tools": "UART, booting u-boot on microSD",
        "SOC": "Exynos 4412",
    },
    {
        "Name" : "Samsung Galaxy SIII (GT-I9300)",
        "Bootloader" : "U-Boot with nonfree and signed BL1",
        "Formfactor": "Smartphone",
        "Debug tools": "UART, booting u-boot on microSD",
        "SOC": "Exynos 4412",
    },
    {
        "Name" : "Samsung Galaxy SIII 4G (GT-I9305)",
        "Bootloader" : "Stock nonfree and signed bootloader (S-Boot 4.0)",
        "Formfactor": "Smartphone",
        "Debug tools": "UART, booting u-boot on microSD",
        "SOC": "Exynos 4412",
    },
    {
        "Name" : "Samsung Galaxy SIII 4G (GT-I9305)",
        "Bootloader" : "U-Boot with nonfree and signed BL1",
        "Formfactor": "Smartphone",
        "Debug tools": "UART, booting u-boot on microSD",
        "SOC": "Exynos 4412",
    },
    {
        "Name" : "Samsung Nexus S (GT-I902x)",
        "Formfactor": "Smartphone",
        "Debug tools": "UART",
        "Bootloader": "Stock nonfree and signed bootloader",
    },
    {
        "Name" : "Samsung Galaxy Note II (GT-N7100)",
        "Formfactor": "Smartphone",
        "Bootloader" : "Stock nonfree and signed bootloader (S-Boot 4.0)",
        "Debug tools": "UART, booting u-boot on microSD",
        "SOC": "Exynos 4412",
    },
    {
        "Name" : "Samsung Galaxy Note II 4G (GT-N7105)",
        "Formfactor": "Smartphone",
        "Bootloader" : "U-Boot with nonfree and signed BL1",
        "Debug tools": "UART, booting u-boot on microSD",
        "SOC": "Exynos 4412",
    },

###########
# Tablets #
###########
    {
        "Name" : "Samsung Galaxy Note 8.0 GSM (GT-N5100)",
        "Formfactor": "Tablet",
        "Bootloader" : "Stock nonfree and signed bootloader (S-Boot 4.0)",
        "Debug tools": "UART, cover removed, booting u-boot on microSD (full disassembly required)",
    },
    {
        "Name" : "Samsung Galaxy Tab 2 7.0 GSM (GT-P3100)",
        "Formfactor": "Tablet",
        "Bootloader" : "Stock nonfree and signed bootloader",
        "Debug tools": "Cover removed. Note that I've only 1 USB cable for it.",
        "Accessories": "USB host adapter, External SD card adapter",
        "SOC" : "OMAP4430",
    },
]

COMPUTERS_FORMFACTORS = []

for computer in COMPUTERS:
    formfactor = computer.get("Formfactor", "Unknown")

    if formfactor not in COMPUTERS_FORMFACTORS:
        COMPUTERS_FORMFACTORS.append(formfactor)
