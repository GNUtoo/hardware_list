#!/usr/bin/env python
# -*- coding: utf-8 -*- #
# Copyright (C) 2020 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import re

class PciDevice(object):
    def __init__(self, vendor_id, product_id, class_id=None):
        self.vendor_id = str(vendor_id)
        self.product_id = str(product_id)

        if class_id != None:
            self.class_id = str(class_id)
        else:
            self.class_id = None

        self.__vendor_str__ = None
        self.__product_str__ = None
        self.__class_str__ = None

    def __find_vendor_product_str__(self):
        # Use caching
        if self.__vendor_str__ and self.__product_str__:
            return self.__vendor_str__, self.__product_str__

        vendorfound = False
        vendor = ""
        product = ""

        # TODO: does the path needs to be configurable?
        pci_ids = open('/usr/share/hwdata/pci.ids', 'r')
        for line in pci_ids:
            if not vendorfound and line.startswith(self.vendor_id):
                vendorfound = True
                vendor = line
                vendor = re.sub(self.vendor_id + " *", '', vendor)
                vendor = re.sub("\n$", '', vendor)
            # vendor ids
            elif vendorfound and re.match("^[0-9a-f]{4}", line):
                # new vendor => device ids not found
                return vendor, None
            # subsystem ids
            elif line.startswith("\t\t"):
                pass
            # devices ids
            elif vendorfound and line.startswith("\t" + str(self.product_id)):
                product = line
                product = re.sub("\t" + str(self.product_id) + " *", '',
                                 product)
                product = re.sub("\n$", '', product)

                self.__vendor_str__ = vendor
                self.__product_str__ = product
                return vendor, product
        return None, None

    def __find_class_str__(self):
        # TODO: really implement it
        if self.class_id != None:
            if self.class_id == "0300":
                return "VGA compatible controller"
        return "[TODO: implement PCI class IDs]"

    def vendor_str(self):
        vendor_str, product_str = self.__find_vendor_product_str__()
        class_str = self.__find_class_str__()
        return vendor_str

    def product_str(self):
        vendor_str, product_str = self.__find_vendor_product_str__()
        class_str = self.__find_class_str__()
        return product_str

    # Can be used in a search engine
    def lspci(self):
        vendor_str, product_str = self.__find_vendor_product_str__()
        class_str = self.__find_class_str__()
        if vendor_str and product_str and class_str:
            lspci_format = class_str + ": " + vendor_str + " " + product_str
            return lspci_format
        else:
            return None

    # TODO:
    # Are search engines indexing entries with lspci -nn too?
    # If so implement the following:
    # def lspci_nn_format():
    #   pass

# TODO: Add testing
# 1002:9710
# lspci:     01:05.0 VGA compatible controller: Advanced Micro Devices, Inc. [AMD/ATI] RS880 [Radeon HD 4200]
# lspci -nn: 01:05.0 VGA compatible controller [0300]: Advanced Micro Devices, Inc. [AMD/ATI] RS880 [Radeon HD 4200] [1002:9710]
