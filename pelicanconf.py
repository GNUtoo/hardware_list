#!/usr/bin/env python
# -*- coding: utf-8 -*- #
# Copyright (C) 2020 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from __future__ import unicode_literals

# Import files with the data to create this website
import os
import sys
sys.path.append(os.path.dirname(__file__))
from data.give_and_get import *
from data.testing.computers import *
from data.testing.fpgas import *
from data.testing.gpus import *

AUTHOR = "Denis 'GNUtoo' Carikli"
SITENAME = "GNUtoo's website"
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = None

# Social widget
SOCIAL = None

DEFAULT_PAGINATION = 3

RELATIVE_URLS = True

# Do not generate pages like index, etc automatically
DIRECT_TEMPLATES = []

TEMPLATE_PAGES = {
    'templates/index.html' : 'index.html',
    'templates/give_and_get.html': 'give_and_get.html',
    'templates/computers.html': 'testing/computers.html',
    'templates/fpgas.html': 'testing/fpgas.html',
    'templates/gpus.html': 'testing/gpus.html',
}
